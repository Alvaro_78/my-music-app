import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { map } from 'rxjs/operators'
 
@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor( private http: HttpClient ) {
    console.log('Spotify service listo')
   }

    getQuery( query: string ) {

      const url = `https://api.spotify.com/v1/${ query }`

      const headers = new HttpHeaders({
        'Authorization': 'Bearer BQCAYxMxD0luzf93gDkrPwS_JvAbW-qTudKeUwU2_kbv-BKE6v3ruIcf_f9FZMuxmTmG22QZRBWw_2uM2jNsF-krx-_5ZiII6-7YBewTW2vKGnSe0V5a6V4wk_OnBBdA5UkZtvfQ2TuY'
      })
  
      return this.http.get(url, { headers })

    }

   getNewRelease() {

    return this.getQuery( 'browse/new-releases?offset=0&limit=10' )
        .pipe( map( data => data['albums'].items))
   }

   getArtista(termino: string) {

    return this.getQuery( `search?q=${ termino }&type=artist&market=US&limit=20&offset=5`)
        .pipe( map( data => data['artists'].items))
   }
}
